#!/bin/bash
set -e

__author="Jesse Kleve"
__version=0.1.1
__name="buddy"
__usage="""$__name v$__version

Context Management
------------------
List contexts
$ $__name list

Load a context
$ eval \$($__name load <ctx>)

Edit a context (must load after)
$ $__name edit <ctx>

Delete a context
$ $__name delete <ctx>

Save all contexts to your dotfiles
$ $__name save


Developer Commands
------------------
Build
$ $__name build

Test
$ $__name test

Deploy
$ $__name deploy
"""

version() {
  echo "v$__version"
}

help() {
  echo "$__usage"
}

debug() {
  # echo "$@"
  return
}

BUDDY_HOME="$HOME/.local/share/buddy"
BUDDY_GLOBALS="$HOME/.local/share/buddy/globals.sh"
if [ -n "$DOTFILES" ]; then
  BUDDY_HOME="$DOTFILES/.local/share/buddy"
  BUDDY_GLOBALS="$DOTFILES/.local/share/buddy/globals.sh"
fi

BUDDY_DEFAULTS="
# used for '$__name build'
export BUDDY_BUILD=\"\"

# used for '$__name test'
export BUDDY_TEST=\"\"

# used for '$__name deploy'
export BUDDY_DEPLOY=\"\"

# Create aliases for the context here
#alias my_alias=\"\"
"
BUDDY_GLOBAL_DEFAULTS="
# Create global aliases available to all contexts here
#alias my_global_alias=\"\"
"

build() {
  debug "Running $BUDDY_BUILD"
  eval "$BUDDY_BUILD"
}

test() {
  debug "Running $BUDDY_TEST"
  eval "$BUDDY_TEST"
}

deploy() {
  debug "Running $BUDDY_DEPLOY"
  eval "$BUDDY_DEPLOY"
}

# Edit a context
#
# $1 : editor for editing files
# $2 : context file path
edit_file() {
  local ctx_file="$1"
  if [ "$EDITOR" = "/ide/bin/remote-cli/gitpod-code" ]; then
    "$EDITOR" --wait "$ctx_file"
  else
    "$EDITOR" "$ctx_file"
  fi
}

edit() {
  local ctx_file="$BUDDY_HOME/$1.sh"

  if [ ! -f "$ctx_file" ]; then
    mkdir -p "$BUDDY_HOME"
    echo "$BUDDY_DEFAULTS" > "$ctx_file"
  fi

  # debug "Editing $ctx_filepath"
  edit_file $ctx_file
}

edit_globals() {
  edit "globals"
}

load() {
  local ctx_file="$BUDDY_HOME/$1.sh"

  if [ ! -f "$ctx_file" ]; then
    mkdir -p "$BUDDY_HOME"
    echo "$BUDDY_DEFAULTS" > "$ctx_file"
    edit_file "$ctx_file"
  fi

  if [ -f "$BUDDY_GLOBALS" ]; then
    cat "$BUDDY_GLOBALS"
  fi
  cat "$ctx_file"
}

delete() {
  # buddy rm <ctx>?
  local ctx="$1"
  rm "$BUDDY_HOME/$ctx.sh"
}

# @todo we're going to run into troubles when we're behind main and can't push without pulling first
save() {
  pushd "$BUDDY_HOME" > /dev/null
  if ! git rev-parse --is-inside-work-tree > /dev/null ; then
    echo "Error: $BUDDY_HOME isn't a git repo"
    exit 22
  fi
  git add .
  git commit
  git push
  popd > /dev/null
}

list() {
  ls "$BUDDY_HOME"/*.sh
}

# show() {
#   # echo BUDDY_CURRENT_CTX
#   # cat $BUDDY_HOME/$BUDDY_CURRENT_CTX.sh
# }

case "$1" in
  *) "$@";;
  # @todo restrict the use of the context name "globals"
esac

# @todo add globals to all contexts
