#!/bin/bash
set -e

main() {
  local dotfiles_dir
  dotfiles_dir=$(readlink -f "$0" | xargs dirname)

  cd "$HOME"
  ln -sf "$dotfiles_dir"/.gitconfig .gitconfig
  ln -sf "$dotfiles_dir"/.inputrc .inputrc

  # bash
  ln -sf "$dotfiles_dir"/.bash_aliases .bash_aliases
  for f in $(ls "$dotfiles_dir/.bashrc.d"); do
    cp "$dotfiles_dir/.bashrc.d/$f" ".bashrc.d/$f"
  done

  # buddy (experimental)
  sudo ln -sf "$dotfiles_dir"/buddy.sh /usr/local/bin/buddy

  # replace git email if this is for work
  if echo "$GITPOD_WORKSPACE_CONTEXT" | jq '.repository.cloneUrl' | grep -q belvederetrading; then
    sed -i 's|email = jesse.kleve@gmail.com|email = jkleve@belvederetrading.com|' .gitconfig
  fi
}

main
