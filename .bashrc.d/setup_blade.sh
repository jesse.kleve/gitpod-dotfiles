## setup_blade: Setup dev environment for developing blade.
#
# You should refresh clangd after running this command.
#
setup_blade() {
    bazel build //bt/modeling/liquidity_profile/proto:liquidity_profile_cpp_proto
    bazel run :refresh_compile_commands
}
