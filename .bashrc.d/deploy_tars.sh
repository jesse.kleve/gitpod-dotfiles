## deploy_tars: Deploy tribe tar packages to Artifactory.
#
# Deploy a tar or tars to Artifactory for dev testing.
#
deploy_tars() {
    ./iac/ci-cd/deployment/tars.sh deploy_specific_tars "$1"
}
