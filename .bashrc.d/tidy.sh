## tidy: Run clang tidy on targets.
#
tidy() {
    bazel build --config=clang-tidy "$1"
}
