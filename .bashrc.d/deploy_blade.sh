## deploy_blade: Deploy the blade package to Artifactory.
#
# Deploy to Artifactory for dev testing.
#
deploy_blade() {
    ./iac/ci-cd/deployment/tars.sh deploy_specific_tars //bt/execution/blade/...
}
