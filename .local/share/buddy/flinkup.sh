
# used for 'buddy build'
export BUDDY_BUILD="bazel build //bt/market_data/normalized/java:security_data_normalizer"

# used for 'buddy test'
export BUDDY_TEST="bazel test --test_output=errors //bt/market_data/normalized/java:security_data_normalizer"

# used for 'buddy deploy'
export BUDDY_DEPLOY="./iac/ci-cd/deployment/jars.sh deploy_all_jars"

# Create aliases for the context here
#alias my_alias=""

