
# used for 'buddy build'
export BUDDY_BUILD=""

# used for 'buddy test'
export BUDDY_TEST=""

# used for 'buddy deploy'
export BUDDY_DEPLOY=""

# Create aliases for the context here
#alias my_alias=""


run_cc_tests() {
  local targets=$(bazel query 'kind(cc_test, //bt/...)')
  for t in $targets; do
    if bazel test --test_output=errors "$t"; then
      echo "$t" >> good.txt
    else
      echo "$t" >> bad.txt
    fi
  done
}
