export BUDDY_BUILD="bazel build --compilation_mode=dbg //bt/execution/blade/cpp/app:blade_execution_cpp_app"
export BUDDY_TEST="bazel test --compilation_mode=dbg --test_output=errors //bt/execution/cpp/..."
export BUDDY_DEPLOY="./iac/ci-cd/deployment/tars.sh deploy_specific_tars //bt/execution/blade/cpp/app/..."
